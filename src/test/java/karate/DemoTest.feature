Feature: User API tests

  Background:
    * url 'https://jsonplaceholder.typicode.com'

  Scenario: Get all users
    Given path 'users'
    When method GET
    Then status 200