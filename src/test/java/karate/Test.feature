Feature: User API tests

  Background:
    * url 'https://reqres.in/api'

  Scenario: Get a user
    Given path 'users', '2'
    When method GET
    Then status 200
    And match response.data.id == 2
