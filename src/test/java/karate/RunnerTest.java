package karate;

import com.intuit.karate.junit5.Karate;

public class RunnerTest {

    @Karate.Test
    Karate testOne() {
           // currently hardcoded feature file path
           // to run all feature files - classpath:karate/*.feature
           return Karate.run("classpath:karate/Test.feature");
        }
    }

